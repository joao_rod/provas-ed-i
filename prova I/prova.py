from random import randint

# função que sorteia os valores para cada linha e retorna um vetor já ordenado
def Sort(num_min, num_max):
	linha = []
	while len(linha) != 5:
		aux = randint(num_min, num_max)
		if aux not in linha:
			linha.append(aux)
	return sorted(linha)

# vetor contendo todos os valores da cartela
cartela = [
	Sort(1, 15),
	Sort(16, 30),
	Sort(31, 45),
	Sort(46, 60),
	Sort(61, 75)
]

stop = 0
aux_list = []
while True:
	num_sort = randint(1, 75)

	# condição para não se repetir números no sorteio do bingo
	while num_sort in aux_list:
		num_sort = randint(1, 75)
	aux_list.append(num_sort)
	print('-*-' * 15)
	print(f'\nO número sorteado foi: {num_sort}\n')

	# checa se o número sorteado existe na cartela
	for i in range(len(cartela)):
		for j in range(len(cartela[i])):
			if num_sort == cartela[i][j]:
				cartela[i][j] = 'X'
				print(f'Você possui o número {num_sort} na {i + 1}ª linha, coluna {j + 1}\n')
				stop += 1

	# imprime em formato de tabela
	for i in range(len(cartela)):
		for j in range(len(cartela[i])):
			print(f'{cartela[i][j]}\t', end='')
		print()

	# parar o loop
	if stop == 25:
		print('\nParabéns...Você não ganhou nada com isso!!!')
		break

'''numeros que foram sorteados ==> "esse dado é só para a checar se os 
números sorteados se repetem ou não"'''
print(f'\n\n\n{sorted(aux_list)}')
